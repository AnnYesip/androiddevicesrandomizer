//
//  ViewController.swift
//  androidDevicesRandomizer
//
//  Created by Ann Yesip on 19.07.2021.
//

import UIKit

class ViewController: UIViewController {
  
  let android9 = ["Huawei P30 Pro","Google Pixel 3", "Doogee S95 Pro" ]
  let android10 = ["Realme X3 Superzoom","Samsung Galaxy A71", "Samsung Galaxy Note 20 Ultra", "Xiaomi Mi 10 Pro"]
  let android11 = ["Google Pixel 5", "Samsung Galaxy S21 Ultra 5G", "Samsung Galaxy S20"]
  
  var androidPhones = [String]()
  
  var androidVersion: String = ""
  
  //  MARK: IBOutlet & IBAction
  @IBOutlet weak var PhoneLabel: UILabel!
  
  @IBAction func TapToGetPhone(_ sender: Any) {
    guard let randomEl = androidPhones.randomElement() else { return }
    findAndroidVersion(phone: randomEl)
    PhoneLabel.text = "\(randomEl) \n(\(androidVersion))"
  }
  
  //  MARK: ViewDidLoad
  override func viewDidLoad() {
    super.viewDidLoad()
    androidPhones = android9 + android10 + android11
  }
  
  //  MARK: func
  func findAndroidVersion(phone: String){
    switch phone {
    case android9.first(where:{ $0 == phone}):
      androidVersion = "Android 9"
    case android10.first(where:{ $0 == phone}):
      androidVersion = "Android 10"
    case android11.first(where:{ $0 == phone}):
      androidVersion = "Android 10"
    default:
      androidVersion = "unknown Android version"
    }
  }
  
}



